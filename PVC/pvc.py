#!/usr/bin/python
import os
import ConfigParser
import Converters

# Class to implement the Converters and Hook everything together.
class PVC:
	__thisDir = os.path.dirname(os.path.realpath(__file__))

	def __init__(self, presets_path):
		os.chdir(PVC.__thisDir)

		# Defaults:
		default_values =\
		{
			"order": "|||",
		}

		# Read the config file:
		self.__presets = ConfigParser.ConfigParser(default_values)
		self.__presets.read(presets_path)

		# Instantiate the Converter.
		self.__converter = Converters.FFMPEG()

		self.__preset_names = self.__presets.sections()
		self.__preset_names.sort(key=lambda x: self.__presets.get(x, "order")+"____"+x)

		self.__default_options = dict(self.__presets.items(self.__preset_names[0])) if self.__preset_names else dict()
		self.__bundles = []
		self.__combine = False

	def __add_bundle(self, path):
		# Try to add bundle for path. Ignore invalid paths.
		self.__bundles.append(Converters.Bundle(os.path.realpath(path), self.__default_options))

	def add_paths(self, paths):
		# Build a list of bundles to handle.
		for path in paths:
			if path.startswith("-"):
				if path=="-combine":
					self.__combine = True
			else:
				self.__add_bundle(os.path.realpath(path))

	def select_presets(self):
		# Select conversion options:
		preset_names_lower = [preset_name.lower() for preset_name in self.__preset_names]

		# Print the choices:
		print "Pick a preset. Press enter for default (" + self.__preset_names[0] + ")."
		for preset_name in self.__preset_names:
			index = self.__preset_names.index(preset_name) + 1
			print " " + str(index) + ") " + preset_name

		for bundle in self.__bundles:
			# Read the input for the preset selection.
			preset_input_text = raw_input("Preset for converting " + bundle.GetShortDescription() + ": ")

			# Correct preset selection.
			try:
				index = int(preset_input_text)
				if 0 < index <= len(self.__preset_names):
					# Pick by index.
					preset_input_text = self.__preset_names[index - 1]
				else:
					# OOB, use default.
					preset_input_text = self.__preset_names[0]
			except:
				if preset_input_text.lower() in preset_names_lower:
					# Specified.
					preset_input_text = self.__preset_names[preset_names_lower.index(preset_input_text.lower())]
				else:
					# Default.
					preset_input_text = self.__preset_names[0]

			# Apply Choice:
			bundle.SetOptions(dict(self.__presets.items(preset_input_text)))

			# print "PVC will convert using " + preset_input_text + "."

	def convert(self):
		# Do Conversion:
		for bundle in self.__bundles:
			err = bundle.GetError()
			if err == "":
				self.__converter.convert(bundle)
			else:
				print "Error: " + err

		if self.__combine:
			o_files = []

			for bundle in self.__bundles:
				o_files += bundle.get_converted_files()

			self.__converter.combine(o_files)


