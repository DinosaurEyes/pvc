import math
import datetime
import os


def size_str(kb):
	if kb > (1024*1024):
		return "%.1f TB" % (float(kb)/(1024*1024))
	elif kb > 1024:
		return "%.1f MB" % (float(kb)/1024)
	else:
		return "%d KB" % kb

def time_str_ms(ms):
	return time_str(datetime.timedelta(milliseconds=ms))

def total_seconds(td):
	return (td.microseconds + 0.0 + (td.seconds + td.days * 24 * 3600) * 10 ** 6) / 10 ** 6

def time_str(time):
	hours, remainder = divmod(time.seconds, 3600)
	minutes, seconds = divmod(remainder, 60)
	if hours > 0:
		return "%d hours %d mins" % (hours, minutes)
	elif minutes > 0:
		return "%d mins %d sec" % (minutes, seconds)
	elif seconds >= 20:
		return "%d secs" % (seconds)
	else:
		hsecs = time.microseconds / 10000
		return "%d.%02d secs" % (seconds, hsecs)

class Progress:

	def __init__(self, bundle, file_path):
		info = os.stat(file_path)
		self.file_path = file_path
		self.progress = 0
		self.bundle = bundle
		self.converted_ms = 0
		self.input_ms = 0
		self.output_ms = bundle.GetOutputFileLengthMS()
		self.output_ms_scalar = bundle.GetOutputFileLengthMultiplier()
		self.input_kb = (info.st_size/1024)
		self.converted_kb = 0
		self.start_time = None
		self.end_time = None
		self.sample_perc = 1

	def set_start_time(self):
		self.start_time = datetime.datetime.now()

	def set_end_time(self):
		self.end_time = datetime.datetime.now()

	def get_conversion_time(self):
		return self.end_time - self.start_time

	def has_input_ms(self):
		return self.input_ms > 0

	def set_input_ms(self, ms):
		self.input_ms = ms
		if self.output_ms > 0:
			self.sample_perc = min( max((float(self.output_ms * self.output_ms_scalar)/float(self.input_ms)), 0), 1)

	def set_converted_ms(self, ms):
		if self.converted_ms == 0:
			self.set_start_time()
		self.converted_ms = ms

	def set_converted_kb(self, kb):
		self.converted_kb = kb

	def get_output_file_ms(self):
		return self.output_ms * self.output_ms_scalar if self.output_ms > 0 else self.input_ms * self.output_ms_scalar

	def get_percentage(self):
		expected_ms = self.get_output_file_ms()
		if expected_ms > 0:
			return 100.0 * min(max(float(self.converted_ms) / float(expected_ms), 0), 1)
		else:
			return 50.0 if self.converted_ms > 0 else 0.0

	def get_progress_str(self):
		expected_ms = self.get_output_file_ms()
		if expected_ms > 0:
			perc_converted = self.get_percentage()*0.01
			perc_str_len = 16
			perc_complete_count = min(int(math.ceil(perc_converted*perc_str_len)), perc_str_len)
			perc_str = ("[]"*perc_complete_count) + ("--"*(perc_str_len-perc_complete_count))
			perc_str += (" %.1f%%" % (perc_converted*100))
			if 0.01 <= perc_converted <= 0.99:
				time_so_far = (datetime.datetime.now()-self.start_time)
				expected_total = datetime.timedelta(seconds=total_seconds(time_so_far)/perc_converted)
				remainder = expected_total - time_so_far
				perc_str += " - " + time_str(remainder) + " remaining"
			else:
				perc_str += "                     "
			return "\r" + perc_str
		else:
			return "" + str(math.floor(self.converted_ms*0.001)) + "seconds"

	def get_completion_stats(self):
		stats_str = "Stats:"

		# Conversion rate.
		rate_str = ""
		output_ms = self.get_output_file_ms()
		if output_ms > 0:
			conv_ms = total_seconds(self.get_conversion_time())*1000
			file_ms = float(output_ms)
			rate_str = (" (%.2fx)" % (file_ms/conv_ms))

		# Conversion Time.
		stats_str += "\n Conversion Time: " + time_str(self.get_conversion_time()) + rate_str

		# Conversion Size.
		stats_str += "\n Output Filesize: " + size_str(self.converted_kb)

		conv_size = float(self.converted_kb)
		input_size = self.sample_perc * float(self.input_kb)
		compression_ratio = (conv_size / input_size) * 100
		stats_str += (" (%.1f%% original size)" % compression_ratio)

		return stats_str

	def converting_success(self):
		self.bundle.add_converted_file(self.file_path)

