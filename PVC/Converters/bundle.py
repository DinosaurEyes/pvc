import os.path


def enum(*sequential, **named):
    enums = dict(zip(sequential, range(len(sequential))), **named)
    return type('Enum', (), enums)

BundleType = enum("Invalid", "File", "Dir")

class Bundle:
	def __init__(self, path, default_options):

		self.__path = path
		self.__type = BundleType.Invalid
		self.__files = []
		self.__outputDir = ""
		self.__dirSuffix = ""
		self.__fileSuffix = ""
		self.__desc = ""
		self.__shortDesc = ""
		self.__error = ""
		self.__options = None
		self.__converted_files = []

		# Find out what type it is.

		# Directory:
		if os.path.isdir(path):
			self.__type = BundleType.Dir

			for root, directories, files in os.walk(path):
				for filename in files:
					self.__add_file(os.path.join(root, filename))

			self.__desc = "Directory: '"+path+"'"
			self.__shortDesc = "[" + os.path.split(path)[1] + "]"
			self.__outputDir = path
			self.__dirSuffix = ""
			self.__fileSuffix = ""

		# File:
		if os.path.isfile(path):
			(pathname, filename) = os.path.split(path)
			self.__type = BundleType.File
			self.__add_file(path)
			self.__desc = "File: '"+filename+"'"
			self.__shortDesc = "[" + filename + "]"
			self.__outputDir = pathname
			self.__dirSuffix = ""
			self.__fileSuffix = ""

		self.SetOptions(default_options)

	def __add_file(self, path):
		# Check if this is a valid file.
		#file_format = os.path.splitext(path)[1]
		self.__files.append(path)

	def SetOptions(self, options):
		self.__options = options
		self.__dirSuffix = ""
		self.__fileSuffix = ""
		if not self.HasOption("cnosuffix"):
			suffix = "_"+self.Option("vformat")+"_"+self.Option("vqual")
			if self.__type is BundleType.Dir:
				self.__dirSuffix = suffix
			else:
				self.__fileSuffix = suffix

	def Option(self, key):
		if key in self.__options:
			return self.__options[key]
		return ""

	def OptionInt(self, key):
		if key in self.__options:
			try:
				opt_int = int(self.__options[key])
				return opt_int
			except:
				return 0
		return 0

	def HasOption(self, key):
		return key in self.__options

	def GetDescription(self):
		return self.__desc

	def GetShortDescription(self):
		return self.__shortDesc

	def GetFiles(self):
		return self.__files

	def GetError(self):
		return self.__error

	def GetOptions(self):
		return self.__options

	def GetOutputDir(self):
		return self.__outputDir + self.__dirSuffix

	def GetOutputFilePath(self, i_path, file_format):
		# Build the output filename and path.
		i_name = os.path.split(i_path)[1]
		(i_name, i_ext) = os.path.splitext(i_name)

		# Rebuild the output path.
		o_name = i_name + self.__fileSuffix
		o_path = os.path.join(self.GetOutputDir(), o_name+"."+file_format)
		return (o_path, o_name)

	def GetOutputFileLengthMS(self):
		return self.OptionInt("cpreview") * 1000

	def GetOutputFileLengthMultiplier(self):
		if self.HasOption("va_rate"):
			return 1/float(self.OptionInt("va_rate"))
		return 1

	def IsValid(self):
		return self.__type.Invalid and len(self.GetFiles()) > 0

	def add_converted_file(self, path):
		# Check if this is a valid file.
		if os.path.exists(path):
			self.__converted_files.append(path)

	def get_converted_files(self):
		return self.__converted_files

