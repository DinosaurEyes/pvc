import subprocess
import sys
import time
from Queue import Queue, Empty
from threading import Thread


class BaseConverter:

	def __init__(self):
		self.onPreConvert = None
		self.onFileConverted = None

	def convert(self, obj):
		print "'convert' Not implemented."

	def combine(self, files):
		print "'combine' Not implemented."

	def converting_start(self):
		pass

	def converting_tick(self):
		pass

	def converting_complete(self):
		pass

	def converting_success(self):
		pass

	def converting_error(self):
		pass

	@staticmethod
	def __wrap_unquoted_strings(text):
		if " " in text and not ('"' in text):
			return '"'+text+'"'
		return text

	def start_process(self, params):
		# Add quotes to any substring that has spaces and is missing quotes.
		params = [BaseConverter.__wrap_unquoted_strings(element) for element in params]

		# Convert to a single command string.
		call_string = ' '.join(params)

		#print call_string

		# Run process.
		#subprocess.call(call_string, shell=True)

		process = subprocess.Popen(call_string, shell=True, universal_newlines=True, stdout=subprocess.PIPE, stderr=subprocess.STDOUT)

		self.converting_start()

		# Poll process for new output until finished.
		prev_same_line = False
		while True:
			lines_in = process.stdout.readline()
			process_finished = process.poll() is not None
			if lines_in:
				# Split lines.
				lines_in = lines_in.replace("\n", "\r")
				lines = filter(None, lines_in.split("\r"))
				for line in lines:
					self.process_exe_line(line)

			self.converting_tick()

			if process_finished:
				# Done and no more text.
				break;
			else:
				# Don't thrash.
				#time.sleep(0.1)
				pass

		output = process.communicate()[0]
		exitCode = process.returncode

		self.converting_complete()

		if (exitCode == 0):
			self.converting_success()
			#return output
		else:
			self.converting_error()
			#raise ProcessException(command, exitCode, output)



	@staticmethod
	def __enqueue_output(out, queue):
		for line in iter(out.readline, b''):
			queue.put(line)
		out.close()