# Classes to expose:
from .ffmpeg import FFMPEG
from .bundle import Bundle

# Version
__version__ = '0.1'