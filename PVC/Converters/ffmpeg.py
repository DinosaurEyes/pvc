import os
import re
import subprocess

from .base import BaseConverter
from .progress import Progress

class FFMPEG(BaseConverter):
	__verbosity = ["quiet","panic","fatal","error","warning","info","verbose","debug","trace"]

	__time_format = "\d\d:\d\d:\d\d\.\d\d"

	# frame=  480 fps=115 q=-0.0 Lsize=     941kB time=00:00:20.01 bitrate= 385.4kbits/s dup=1 drop=0
	__update_pattern = re.compile(r'frame=\s*(?P<framenum>\d+)\s*'\
									'fps=\s*(?P<fps>[\d\.]+)\s*'\
									'q=\s*(?P<q>[\-\d\.]+)\s*'\
									'L?size=\s*(?P<size>\d+)kB\s*'\
									'time=\s*(?P<time>'+__time_format+')\s*'\
									'bitrate=\s*(?P<bitrate>[\d\.]+)\s*')

	__duration_pattern = re.compile(r'Duration:\s*(?P<duration>'+__time_format+')')

	__time_pattern = re.compile(r'(?P<hours>\d\d):(?P<mins>\d\d):(?P<secs>\d\d)\.(?P<hsecs>\d\d)')

	def __init__(self):
		self.__ffmpegLocation = os.path.join(os.path.dirname(os.path.realpath(__file__)), "ffmpeg.exe")
		self.__progress = None

	def convert(self, bundle):
		print bundle.GetDescription()
		files = bundle.GetFiles()
		options = bundle.GetOptions()
		output_dir = bundle.GetOutputDir()

		# Make the output dir if it doesn't exist.
		if not os.path.exists(output_dir) :
			os.mkdir(output_dir)

		#print options

		# Build the command line params for these options:

		# Conversion params:
		c_params = ["-hide_banner", "-v", "info"]

		# Audio Params:
		a_params = []
		if bundle.HasOption("abr") :
			a_params += ["-b:a", bundle.Option("abr")+"k"] #"-c:a", "libvo_aacenc",

			if bundle.HasOption("va_rate"):
				video_rate = bundle.Option("va_rate")
				rate = float(video_rate)
				rates = []

				if rate > 1:
					while rate > 2:
						rates += [2]
						rate /= 2

				rate_str = "atempo="+str(rate)
				for r in rates:
					rate_str += ",atempo="+str(r)

				audio_filter = "\""+rate_str+"\""

				a_params += ["-af", audio_filter]


		# FFMPEG Mode Params:
		ffmpeg_params = ["-y"]#, "-strict", "experimental"

		i = 1
		file_count = str(len(files))
		for i_path in files:

			if bundle.Option("vformat")=="none":
				bundle.add_converted_file(i_path)
				continue

			self.__progress = Progress(bundle, i_path)

			# Input params:
			i_params = ["-i", i_path]

			# Video Params:
			(v_params, filter_params, file_ext) = self.__build_video_params(bundle, i_path)

			# Video Duration Clip.
			if bundle.OptionInt("cpreview") > 0:
				v_params += ["-t", str(bundle.Option("cpreview"))]

			# Output params:
			(o_path, o_name) = bundle.GetOutputFilePath(i_path, file_ext)
			o_params = ["-f", file_ext, "-metadata", "title=\""+o_name+'"', o_path]

			# Collate params
			params = [self.__ffmpegLocation] + c_params + i_params + filter_params + ffmpeg_params + v_params + a_params + o_params

			print "=============================================================="
			print "Creating File ["+str(i)+"/"+file_count+"]: " + o_path
			i += 1

			BaseConverter.start_process(self, params)

			self.__progress.bundle.add_converted_file(o_path)

		print "=============================================================="

	def combine(self, files):
		if len(files) == 0:
			return

		self.__progress = None

		comb_path = os.path.join(os.path.split(files[0])[0], "combine"+os.path.splitext(files[0])[1])

		concat = "\"concat:"

		temp_files = []

		for file in files:
			ts_path = os.path.splitext(file)[0]+".ts"
			temp_files.append(ts_path)
			concat = concat + ts_path + "|"
			params = [self.__ffmpegLocation, "-y", "-i", file, "-c", "copy", "-bsf:v", "h264_mp4toannexb", "-f", "mpegts", ts_path]
			BaseConverter.start_process(self, params)

		concat = concat.rstrip("|")
		concat += "\""

		params = [self.__ffmpegLocation, "-y", "-i", concat, "-c", "copy", "-bsf:a", "aac_adtstoasc", comb_path]
		BaseConverter.start_process(self, params)

		# Remove the temp files.
		for file in temp_files:
			os.remove(file)

	def __build_video_params(self, bundle, i_path):

		# Video Quality
		v_params_qual = []
		if bundle.HasOption("vqual"):
			v_params_qual += ["-crf", bundle.Option("vqual")]
		elif bundle.HasOption("vbr"):
			v_params_qual += ["-b:v", bundle.Option("vbr")+"k"]

		# Video FPS
		v_params_fps = []
		if bundle.HasOption("vfps"):
			v_params_fps += ["-r", bundle.Option("vfps")+"000/1001"]

		# Video Scaling:
		video_filter = ""
		if bundle.HasOption("vheight"):
			video_filter += 'scale=-2:trunc('+bundle.Option("vheight")+'/2)*2'
		elif bundle.HasOption("vwidth"):
			video_filter += 'scale=trunc('+bundle.Option("vwidth")+'/2)*2:-2'
		elif bundle.HasOption("vscale"):
			video_filter += 'scale=trunc(iw*'+bundle.Option("vscale")+')/200*2:-2'
		elif bundle.HasOption("vaspect"):
			ar = bundle.Option("vaspect")
			ar = ar.split(":")
			ar_div = "/".join(ar)
			if len(ar) == 2:
				video_filter += 'crop="\'if(gt(a,'+ar_div+'),trunc(ih*'+ar_div+'/2)*2,trunc(iw/2)*2)\':\'if(gt(a,'+ar_div+'),trunc(ih/2)*2,trunc(iw*'+ar[1]+'/'+ar[0]+'/2)*2)\'"'
		else:
			video_filter += 'scale=trunc(iw/2)*2:-2'

		if bundle.HasOption("vsubs"):
			if bundle.Option("vsubs") != "0":
				sub_filename = os.path.splitext(i_path)[0]
				if os.path.exists(sub_filename + ".srt"):
					sub_filename += ".srt"
				elif os.path.exists(sub_filename + ".sub"):
					sub_filename += ".sub"
				elif os.path.exists(sub_filename + ".sfv"):
					sub_filename += ".sfv"
				else:
					sub_filename = i_path
				sub_filename = '\\\\\\\\'.join(sub_filename.split('\\'))
				sub_filename = '\\\\:'.join(sub_filename.split(':'))
				sub_filename = '\\,'.join(sub_filename.split(','))
				sub_filename = '\\['.join(sub_filename.split('['))
				sub_filename = '\\]'.join(sub_filename.split(']'))
				video_filter += ', subtitles=' + sub_filename + ''

		video_filter = '"' + video_filter + '"'

		#print "VF: "+ video_filter

		video_params = []
		file_ext = ""
		conv_speed_index = bundle.OptionInt("cspeed") if bundle.HasOption("cspeed") else 5

		video_format = bundle.Option("vformat")
		if video_format == "x264":
			conv_speeds = { 1: "veryslow", 2: "slower", 3: "slow", 4: "medium", 5: "medium", 6: "fast", 7: "faster", 8: "veryfast", 9: "superfast", 10:"ultrafast" }
			video_params = ["-c:v", "libx264", "-profile:v", "high", "-level", "3.1", "-preset", conv_speeds[conv_speed_index], "-pix_fmt", "yuv420p"] + v_params_fps + v_params_qual + ["-qmin", "0", "-qmax", "69", "-threads", "16", "-bf", "16", "-refs", "14"]
			file_ext = "mp4"

		elif video_format == "x265":

			conv_speeds = {1: "veryslow", 2: "slower", 3: "slow", 4: "medium", 5: "medium", 6: "fast", 7: "faster", 8: "veryfast", 9: "superfast", 10:"ultrafast"}
			v_params_qual = []
			if bundle.HasOption("vbr"):
				v_params_qual += ["-b:v", bundle.Option("vbr")+"k"]

			# Log Level:
			# 0 error
			# 1 warning
			# 2 info (default)
			# 3 debug
			# 4 full
			x265_params_str = "log-level=1:"
			if bundle.HasOption("vqual"):
				x265_params_str += "crf=" + bundle.Option("vqual") + ":"
			if x265_params_str.endswith(":"):
				x265_params_str = x265_params_str[:-1]

			video_params =  ["-c:v", "libx265"] + \
							["-x265-params", x265_params_str] + \
							["-preset", conv_speeds[conv_speed_index]] + \
							["-pix_fmt", "yuv420p"] + \
							v_params_qual
			file_ext = "mp4"

		elif video_format == "gif":
			video_params = ["-loop", "0"] + v_params_fps
			file_ext = "gif"

			input_video_duration = self.extract_video_info(i_path)

			gif_length = "10"
			if bundle.HasOption("vgiftime"):
				gif_length = bundle.Option("vgiftime")

			video_filter += ",\"setpts="+gif_length+"*PTS/"+input_video_duration+"\""#+v_params_fps

		if bundle.HasOption("va_rate"):
			video_rate = bundle.Option("va_rate")
			video_filter += ",\"setpts=PTS/"+video_rate+"\""

		if bundle.HasOption("cfileext"):
			file_ext = bundle.Option("cfileext")

		filter_params = ["-vf", video_filter]

		return video_params, filter_params, file_ext

	def process_exe_line(self, line):
		if self.__progress is not None:
			if not self.__progress.has_input_ms():
				match = re.search(FFMPEG.__duration_pattern, line)
				if not match is None:
					self.__progress.set_input_ms(FFMPEG.time_to_ms(match.group("duration")))
			else:
				match = re.search(FFMPEG.__update_pattern, line)
				if not match is None:
					framenum = match.group("framenum")
					fps = match.group("fps")
					quan = match.group("q")
					file_size = match.group("size")
					time = match.group("time")
					bitrate = match.group("bitrate")
					self.__progress.set_converted_ms(FFMPEG.time_to_ms(time))
					self.__progress.set_converted_kb(int(file_size))
		else:
			#print line
			return

	@staticmethod
	def time_to_ms(time_str):
		m = re.search(FFMPEG.__time_pattern, time_str)
		if m is None:
			return 0
		else:
			return (int(m.group("hours"))*3600000) + (int(m.group("mins"))*60000) + (int(m.group("secs"))*1000) + (int(m.group("hsecs"))*10)

	def extract_video_info(self, i_path):
		params = [self.__ffmpegLocation, "-i", i_path]

		call_string = ' '.join(params)

		process = subprocess.Popen(call_string, shell=True, universal_newlines=True, stdout=subprocess.PIPE, stderr=subprocess.STDOUT)

		while True:
			lines_in = process.stdout.readline()
			process_finished = process.poll() is not None
			if lines_in:
				# Split lines.
				lines_in = lines_in.replace("\n", "\r")
				lines = filter(None, lines_in.split("\r"))
				for line in lines:
					match = re.search(FFMPEG.__duration_pattern, line)
					if match is not None:
						time_in_sec = FFMPEG.time_to_ms(match.group("duration")) / 1000
						return str(time_in_sec)

			if process_finished:
				break;

		output = process.communicate()[0]

		return "10"


	def converting_start(self):
		if self.__progress is not None:
			self.__progress.set_start_time()

	def converting_tick(self):
		if self.__progress is not None:
			text = self.__progress.get_progress_str()
			print text,

	def converting_complete(self):
		if self.__progress is not None:
			self.__progress.set_end_time()
			print ""
			print "Conversion complete..."

	def converting_success(self):
		if self.__progress is not None:
			print self.__progress.get_completion_stats()

	def converting_error(self):
		print "Error converting file. :("

