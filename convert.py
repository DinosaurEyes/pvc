import sys
import PVC
import os
import time

# Parse the cmd line params.
input_list = sys.argv[:]
del input_list[0]

# Testing. Add default file(s)
if len(input_list) == 0:
	#input_list.append("C:\\home\\video\\test\\test.divx")
	#input_list.append("D:/Video/TV/New/Its Always Sunny In Philadelphia - Season 10/Its.Always.Sunny.in.Philadelphia.S10E02.HDTV.x264-KILLERS.mp4")
	#input_list.append("C:\\home\\video\\test\\1.avi")
	#input_list.append("D:\\Video\\Films\\sub\\Riki-Oh The Story Of Ricky [1991].avi")
	#input_list.append("-combine")
	#input_list.append("C:\\home\\video\\test\\REC_0006_x264_20.mp4")
	#input_list.append("C:\\home\\video\\test\\REC_0008_x264_20.mp4")
	input_list.append("D:\\Video\\Convert\\REC_0006.AVI")
	#input_list.append("C:\\home\\video\\TV\\otgw.mp4")

presets_path = os.path.join(os.path.dirname(os.path.realpath(__file__)), "presets.ini")

pvc = PVC.PVC(presets_path)
pvc.add_paths(input_list)
pvc.select_presets()
pvc.convert()

print "Done!"
time.sleep(3)
