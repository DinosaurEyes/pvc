# PETE'S VIDEO CONVERTER #

Here's my handy video compression toolkit to help with compressing your videos.

Just drag your video file(s) or folder(s) full of videos onto the run.bat file for fast and effective compression.

There are several presets for converting controlled by the presets.ini config file.

Currently using ffmpeg to encode the files.